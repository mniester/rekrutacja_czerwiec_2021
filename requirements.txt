starlette==0.14.2
pydantic==1.8.2
requests==2.25.1
fastapi==0.65.2
uvicorn==0.13.4
