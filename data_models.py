from typing import Optional, List
from pydantic import BaseModel



class Book_model(BaseModel):

    id: str    
    title: str
    authors: List[str]
    publishedDate: int
    categories: List[str]
    average_rating: Optional[int] = None
    ratings_count: Optional[int] = None
