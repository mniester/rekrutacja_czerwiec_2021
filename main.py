from fastapi import FastAPI, status, Query
from typing import Optional, List
from db import DB_access
from data_models import Book_model


app = FastAPI()


@app.get('/', status_code = status.HTTP_200_OK)
def index():
    return {'Foo' : 'Bar'}



@app.get('/books/{book_id}')
def single_id(book_id: str):
    result = books_query([book_id])
    return result



@app.get('/books/', status_code = status.HTTP_200_OK)
def books_query(query: Optional[List[str]] = Query(['all']), sorting = False):
    output = []
    with DB_access() as db:
        if query[0] == 'all':
            for result in db.get_data('all'):
                output.append(result)
        else:
            for q in query:
                for result in db.get_data(q):
                    output.append(result)
    if sorting:
        output.sort(key = lambda x: x[3])
    for o in output:
        yield o


@app.post('/db', status_code = status.HTTP_201_CREATED)
async def db_post(book: Book_model):
    with DB_access() as db:
        db.add_data(book)
