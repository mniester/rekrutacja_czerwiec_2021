import requests

source = 'https://www.googleapis.com/books/v1/volumes'

response = requests.get(url = source, params = {"q": "war"})
books = response.json()['items']

access_keys = ['volumeInfo']
valid_keys = ["id", "title", "authors", "categories", 'publishedDate']
date_keys = ['publishedDate']
target = 'http://0.0.0.0:8000'
db_suffix = '/db'
books_suffix = '/books/' 

def data_cutter(book, access_keys, valid_keys, date_keys):
    id = book['id']
    for ak in access_keys:
        try:
            book = book[ak]
        except KeyError:
            # Here should be error log entry
            pass
    invalid_keys = []
    for key in book.keys():
        if key not in valid_keys:
            invalid_keys.append(key)
        if key in date_keys:
            book[key] = int(book[key][:4])
    [book.pop(key) for key in invalid_keys]
    book['id'] = id
    return book


for b in books:
    data = data_cutter(b, access_keys, valid_keys, date_keys)
    target_db = target + db_suffix
    try:
        requests.post(target_db, json = data)
    except requests.ConnectionError as ce:
        # Here should be error log entry
        pass

target_2 = target + books_suffix
heroku = 'https://gentle-retreat-94825.herokuapp.com/books/'

payload = {'query': ['NCJnAAAAMAAJ', 'Xe1HAAAAMAAJ', 'Wojna makowa tom 1'], 'sorting' : True}

target_3 = target_2 + 'NCJnAAAAMAAJ'

result = requests.get(target_3)
print(result.text)
print(result.url)
