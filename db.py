fake_db = []
hardcoded = [
('NCJnAAAAMAAJ', 'Our War Too', ('Margaret Paton-Walsh',), 2002, ('History',), None, None),
('OEnZAAAAMAAJ', 'Studying War--no More?', ('Brian Wicker',), 1994, ('Religion',), None, None),
('ckD1DwAAQBAJ', 'China’s Good War', ('Rana Mitter',), 2020, ('History',), None, None),
('4y0eAAAAMAAJ', 'War Survival in Soviet Strategy', ('Leon Gouré',), 1976, ('Civil defense',), None, None),
('NqbPDwAAQBAJ', 'Wojna makowa tom 1', ('Rebecca F. Kuang',), 2020, ('Fiction',), None, None),
('iKQfAAAAMAAJ', 'Drives Toward War', ('Edward Chace Tolman',), 1942, ('Psychophysiology',), None, None),
('yMnZAAAAMAAJ', 'Donald Friend, Australian War Artist, 1945', ('Gavin Fry',), 1982, ('Art and war',), None, None),
('SozXAAAAMAAJ', 'Employment and Unemployment in Pre-war and Soviet Russia', ('Susan Myra Kingsbury', 'Mildred Fairchild'), 1932, ('Communism',), None, None),
('_pZoAAAAMAAJ', 'War and Society in Renaissance Florence', ('Charles Calvert Bayley', 'Leonardo Bruni'), 1961, ('Condottieri',), None, None),
('Xe1HAAAAMAAJ', 'The Makers of War', ('Francis Neilson',), 1950, ('Europe',), None, None)]

class DB_access:

    @staticmethod
    def __enter__():
        # Here should be cursor
        # DB_access.db = sql.connect('data_bases/test.db')
        return DB_access
    
    @staticmethod
    def add_data(data):
        fake_entry = (data.id, data.title, tuple(data.authors),
                    data.publishedDate, tuple(data.categories), 
                    data.average_rating, data.ratings_count)
        print(fake_entry)
        fake_db.append(fake_entry)
    
    @staticmethod
    def get_data(*args):
        if fake_db:
            source = fake_db
        else:
            source = hardcoded
        if args[0] == 'all':
            for f in source:
                yield f
        else:
            for query in args:
                for entry in source:
                    for feature in entry:
                        if type(feature) == tuple:
                            for part in feature:
                                if part == query:
                                    yield entry
                        else:
                            if feature == query:
                                yield entry

    @staticmethod
    def __exit__(exc_type, exc_value, exc_traceback):
        # DB_access.db.close()
        pass
